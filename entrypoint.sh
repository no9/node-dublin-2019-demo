#! /bin/sh

# Mount core dump location as root
# mkdir -p /tmp/coredumps
# chmod a+rwx /tmp/coredumps
# echo “/tmp/coredumps/core.%e.%p.%h.%t” > /proc/sys/kernel/core_pattern

# Run node app
node --perf-basic-prof-only-functions --abort-on-uncaught-exception .
