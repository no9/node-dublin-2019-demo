# node-dublin-2019-demo

A demo application to show eBPF with node.js

1. Create a Kubernetes Cluster on IBM Cloud
--* https://cloud.ibm.com/docs/containers?topic=containers-cs_cluster_tutorial
2. Create a devops pipeline
--* https://www.ibm.com/cloud/garage/tutorials/use-develop-kubernetes-app-toolchain
3. Clone the devops pipeline repository to your local machine
4. Replace the index.js, Dockerfile and package.json with the ones from this repository.
5. Commit the code back and the app should automatically deploy to your cluster.
6. On a linux machine install golang
--* https://golang.org/doc/install
7. Create a src folder for the kubectl extension in
--* /go/src/github.com/iovisor/
8. Clone the kubectl-trace to the iovisor folder
--* git clone -b usdt-support https://github.com/No9/kubectl-trace.git
9. In the kubectl-trace folder run make
10. symlink the /bin/kubectl-trace to your bin folder
11. kubectl trace run -u -e 'usdt::dtrace_express:trace { printf("evt:fired %s : %s\n", str(arg0), str(arg1)) ; }' pod/hello-app-756898f7c-n6dvh -a --imagename docker.io/number9/kubectl-trace-bpftrace:ubuntu-19-04-v1.0.5
--* Where pod/X is the name of your pod from `kubectl get pods`

sudo bpftrace -e 'kprobe:do_sys_open { printf("%s: %s\n", comm, str(arg1)) }'
sudo tplist -p 3728
sudo bpftrace -p 3728 event.bt
ab -n 2000 http://localhost:3000/event


kubectl trace run -u -e 'usdt::dtrace_express:trace { printf("evt:fired %s : %s\n", str(arg0), str(arg1)) ; }' pod/hello-app-77874fb9f5-cxbv7 -a --imagename docker.io/number9/kubectl-trace-bpftrace:ubuntu-19-04-v1.0.5
ab -n 200 http://158.177.196.102:32194/event



