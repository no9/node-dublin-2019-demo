# Check out https://hub.docker.com/_/node to select a new base image
FROM number9/libstapsdt

# Set to a non-root built-in user `node`
COPY ./apport /usr/share/apport/apport

RUN chmod +x /usr/share/apport/apport

USER node

# Create app directory (with user `node`)
RUN mkdir -p /home/node/app

WORKDIR /home/node/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY --chown=node package*.json ./

RUN npm install

# Bundle app source code
COPY --chown=node . .

RUN npm install

# Bind to all network interfaces so that it can be mapped to the host OS
ENV HOST=0.0.0.0 PORT=3000

EXPOSE ${PORT}

RUN chmod +x ./entrypoint.sh

CMD [ "./entrypoint.sh" ]
